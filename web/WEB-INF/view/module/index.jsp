<%-- 
    Document   : hola
    Created on : 09-ene-2017, 18:54:23
    Author     : alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="msg" class="java.lang.String" scope="request" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Module Controller</h1>
        <h2>Método <%= msg %> </h2>
    </body>
</html>

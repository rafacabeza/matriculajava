<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Study"%>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Alta de estudios </h1>
    <form action="<%= request.getContextPath()%>/study/insert" method="post">
        <label>Codigo</label><input name="code" type="text"><br>
        <label>Abreviatura</label><input name="abreviation" type="text"><br>
        <label>Nombre</label><input name="name" type="text"><br>
        <label>Nombre Corto</label><input name="shortName" type="text"><br>
        <label>Nombre Corto</label><input value="Enviar" type="submit"><br>
    </form>     
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>

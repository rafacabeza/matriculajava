<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Study"%>
<%@include file="/WEB-INF/view/header.jsp" %>
<jsp:useBean id="study" class="model.Study" scope="request"/>  

<div id="content">
    <% if(study.getId() == 0){%>
    <h1> Registro no encontrado </h1>
    <% } else { %>
    <h1>Edici�n de estudios </h1>
    <form action="<%= request.getContextPath()%>/study/update" method="post">
        <input name="id" type="hidden" value="<%= study.getId()%>"><br>
        <label>Codigo</label><input name="code" type="text" value="<%= study.getCode()%>"><br>
        <label>Abreviatura</label><input name="abreviation" type="text" value="<%= study.getAbreviation() %>"><br>
        <label>Nombre</label><input name="name" type="text" value="<%= study.getName()%>"><br>
        <label>Nombre Corto</label><input name="shortName" type="text" value="<%= study.getShortName() %>"><br>
        <label>Nombre Corto</label><input value="Guardar" type="submit"><br>
    </form>    
    <% } %>
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Teacher;

/**
 *
 * @author usuario
 */
public class TeacherDAO extends BaseDAO {

    private static final Logger LOG = Logger.getLogger(TeacherDAO.class.getName());

    public TeacherDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }


    public ArrayList<Teacher> getAll() {
        PreparedStatement stmt = null;
        ArrayList<Teacher> teachers = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from teachers");
            ResultSet rs = stmt.executeQuery();
            teachers = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Teacher teacher = new Teacher();
                teacher.setId(rs.getLong("id"));
                teacher.setName(rs.getString("name"));
                teacher.setSurname(rs.getString("surname"));
                teacher.setPhone(rs.getInt("phone"));

                teachers.add(teacher);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return teachers;
    }
    
}

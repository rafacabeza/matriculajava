/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.util.ArrayList;
import java.util.logging.Logger;
import model.Teacher;
import persistence.TeacherDAO;


/**
 *
 * @author usuario
 */
public class TeacherController extends BaseController {

    private static final Logger LOG = Logger.getLogger(TeacherController.class.getName());
    private TeacherDAO teacherDAO;

    public void index() {

        //objeto persistencia
        teacherDAO = new TeacherDAO();
        ArrayList<Teacher> teachers = null;

        //leer datos de la persistencia
        synchronized (teacherDAO) {
            teachers = teacherDAO.getAll();
        }
        request.setAttribute("teachers", teachers);
        String name = "index";
        LOG.info("En TeacherController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/teacher/index.jsp");
    }
}
